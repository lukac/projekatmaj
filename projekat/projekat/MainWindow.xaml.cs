﻿using Microsoft.Win32;
using projekat.model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace projekat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Point startPoint = new Point();


        protected double lastX = 0;
        protected double lastY = 0;
        protected double currentX = 0;
        protected double currentY = 0;
        protected double promenaSkrolaHorizontalna = 0;
        protected double promenaSkrolaVertikalna = 0;
        protected double taboviSirina = 0;
        protected int prethodnoSelektovan = 0;
        protected Point lokacijaKlikaNaMapi = new Point();


        public ObservableCollection<Mapa> Mape
        {
            get;
            set;
        }

        public ObservableCollection<Tip> Tipovi
        {
            get;
            set;
        }

        public ObservableCollection<Etiketa> Etikete
        {
            get;
            set;
        }

        public ObservableCollection<Manifestacija> Manifestacije2
        {
            get;
            set;
        }

        public ObservableCollection<Manifestacija> ListaManifestacija
        {
            get;
            set;
        }

        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.DataContext = this;

            // Kolekcija mapa
            Mape = new ObservableCollection<Mapa>();

            // Kreirako 4 mape
            Mapa mapa1 = new Mapa() { Naziv = "Kosjerić", SlikaMape = "../../Images/mapa_kosjeric.png", Platnoo = new Canvas(), UcitanaSlikaMape = new Image() };
            Mapa mapa2 = new Mapa() { Naziv = "Osijek", SlikaMape = "../../Images/mapa_osjiek.PNG", Platnoo = new Canvas(), UcitanaSlikaMape = new Image() };
            Mapa mapa3 = new Mapa() { Naziv = "Valjevo", SlikaMape = "../../Images/mapa_valjevo.png", Platnoo = new Canvas(), UcitanaSlikaMape = new Image() };
            Mapa mapa4 = new Mapa() { Naziv = "Novi Sad", SlikaMape = "../../Images/mapa_novisad.png", Platnoo = new Canvas(), UcitanaSlikaMape = new Image() };

            mapa1.UcitanaSlikaMape.Source = LoadImageFromFile(mapa1.SlikaMape);
            mapa2.UcitanaSlikaMape.Source = LoadImageFromFile(mapa2.SlikaMape);
            mapa3.UcitanaSlikaMape.Source = LoadImageFromFile(mapa3.SlikaMape);
            mapa4.UcitanaSlikaMape.Source = LoadImageFromFile(mapa4.SlikaMape);

            mapa1.Platnoo.Children.Add(mapa1.UcitanaSlikaMape);
            mapa2.Platnoo.Children.Add(mapa2.UcitanaSlikaMape);
            mapa3.Platnoo.Children.Add(mapa3.UcitanaSlikaMape);
            mapa4.Platnoo.Children.Add(mapa4.UcitanaSlikaMape);

            // dodajemo 4 mape u kolekciju
            Mape.Add(mapa1);
            Mape.Add(mapa2);
            Mape.Add(mapa3);
            Mape.Add(mapa4);


            

            // tipovi
            Tipovi = new ObservableCollection<Tip>();
            // etikete
            Etikete = new ObservableCollection<Etiketa>();

            // Ovo je glavna lista
            ListaManifestacija = new ObservableCollection<Manifestacija>();



            List<UIElement> listaElemenata = new List<UIElement>();

            foreach (UIElement el in Mape[0].Platnoo.Children)
            {
                listaElemenata.Add(el);
            }
            Mape[0].Platnoo.Children.Clear();

            foreach (UIElement el in listaElemenata)
            {
                platno.Children.Add(el);
            }
        }

        private void Izadji_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void TreeViewItem_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem tvi = (TreeViewItem)sender;
            //bool roditelj = tvi.HasItems;

            //if (roditelj)
            //    btnKreirajManifestaciju.IsEnabled = true;
            //else
            //    btnKreirajManifestaciju.IsEnabled = false;

            e.Handled = true;
        }

        private void dropdown_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            prethodnoSelektovan = dropdown.SelectedIndex;
        }

        // podesava glavnu listu ListaManifestacija u zavisnosti od selektovane mape
        private void dropdown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            listView_manifestacije.ItemsSource = Mape[dropdown.SelectedIndex].Manifestacije;
            ListaManifestacija = Mape[dropdown.SelectedIndex].Manifestacije;

            // preuzeti uielement prethodno selektovanog i ukloniti ga iz canvasa

            //Mape[dropdown.SelectedIndex].Platnoo.Children.Clear();
            //UIElement element = platno.Children[0];
            //platno.Children.Clear();
            //Mape[dropdown.SelectedIndex].Platnoo.Children.Add(element);


            //Mape[dropdown.SelectedIndex].UcitanaSlikaMape.Source = LoadImageFromFile(Mape[dropdown.SelectedIndex].SlikaMape);
            
            


            //
            if (dropdown.SelectedIndex != prethodnoSelektovan)
            {
                List<UIElement> listaElemenata = new List<UIElement>();

                // najpre vratimo
                foreach (UIElement el in platno.Children)
                {
                    listaElemenata.Add(el);
                }
                platno.Children.Clear();

                foreach (UIElement el in listaElemenata)
                {
                    Mape[prethodnoSelektovan].Platnoo.Children.Add(el);
                }

                listaElemenata.Clear();

                // potom preuzmemo nove

                foreach (UIElement el in Mape[dropdown.SelectedIndex].Platnoo.Children)
                {
                    listaElemenata.Add(el);
                }
                Mape[dropdown.SelectedIndex].Platnoo.Children.Clear();

                foreach (UIElement el in listaElemenata)
                {
                    platno.Children.Add(el);
                }
            }

            //platno.Children.Add(Mape[dropdown.SelectedIndex].UcitanaSlikaMape);

            //




            // postavljanje platna
            //List<UIElement> elementiPlatna = new List<UIElement>();

            //foreach (UIElement el in Mape[dropdown.SelectedIndex].Platnoo.Children)
            //    elementiPlatna.Add(el);

            //Mape[dropdown.SelectedIndex].Platnoo.Children.Clear();

            //Mape[dropdown.SelectedIndex].Platnoo.Children.Add(Mape[dropdown.SelectedIndex].UcitanaSlikaMape);
            //foreach (UIElement el in elementiPlatna)
            //    platno.Children.Add(el);
            // end of postavljanje platna
        }

        private void BtnKreirajManifestaciju_Click(object sender, RoutedEventArgs e)
        {
            ListaManifestacija.Add(new Manifestacija() { Oznaka = "xx01", Ime = "Nova Manifestacija " + (ListaManifestacija.Count + 1) });
        }

        private void BtnUkloniSelektovano_Click(object sender, RoutedEventArgs e)
        {
            List<Manifestacija> listaZaBrisanje = new List<Manifestacija>();
            IList selektovane = listView_manifestacije.SelectedItems;

            // uzimamo selektovane objekte iz liste
            foreach (Manifestacija m in selektovane)
            {
                foreach (Etiketa et in m.Etiketee)
                    MessageBox.Show(et.Oznaka);

                listaZaBrisanje.Add(m);
            }
            // sve objekte unutar listeZaBrisanje uklanjamo iz ListaManifestacija
            foreach (Manifestacija m in listaZaBrisanje)
                ListaManifestacija.Remove(m);
        }

        //private void BtnKreirajMapu_Click(object sender, RoutedEventArgs e)
        //{
        //    int brojMapa = Mape.Count;

        //    if (brojMapa < 4)
        //    {
        //        Mapa m = new Mapa() { Naziv = "Nova Mapa " + (brojMapa + 1) };
        //        m.Manifestacije.Add(new Manifestacija() { Oznaka = "xx01", Ime = "Nova Manifestacija" });
        //        Mape.Add(m);
        //    }
        //    else
        //    {
        //        MessageBox.Show("Nije moguće dodati više od 4 mape", "Dostignut maksimalan broj mapa");
        //    }
        //}

        // POCETAK KODA ZA OBRADU PODATAKA
        //private int GetTreeViewItemParentIndex(TreeViewItem Item)
        //{
        //    int index = 0;
        //    MessageBox.Show("Metoda 1");
        //    foreach (var _item in trvManifestacije.Items)
        //    {
        //        MessageBox.Show("Metoda 2");
        //        if (_item == Item.Parent)
        //        {
        //            return index;
        //        }
        //        index++;
        //    }
        //    return 0;
        //    //throw new Exception("No parent window detected");
        //}
        
        private void UcitavajIkonicu_Click(object sender, RoutedEventArgs e)
        {
            Manifestacija st = (Manifestacija)listView_manifestacije.SelectedItem;

            OpenFileDialog dg = new OpenFileDialog
            {
                Title = "Otvori sliku",
                Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                "Portable Network Graphic (*.png)|*.png"
            };

            // TODO 1: Proveriti da li ovde moze doci do izuzetka
            dg.ShowDialog();
                
            st.Ikonica = dg.FileName;
            st.UcitanaIkonica = new Image();
            st.UcitanaIkonica.Source = LoadImageFromFile(st.Ikonica);
            st.IkonicaZaPrikaz = LoadImageFromFile(st.Ikonica);
        }

        private void BtnUkloniTip_Click(object sender, RoutedEventArgs e)
        {

            List<Tip> listaZaBrisanje = new List<Tip>();
            IList selektovane = listview_tipovi.SelectedItems;

            // uzimamo selektovane objekte iz liste
            foreach (Tip tip in selektovane)
                listaZaBrisanje.Add(tip);

            foreach (Mapa mp in Mape)
                foreach (Manifestacija mf in mp.Manifestacije)
                    foreach (Tip t in listaZaBrisanje)
                        if (mf.Tip == t)
                            mf.Tip = null;
            
            foreach (Tip tp in listaZaBrisanje)
                if (Manifestacija.ListaTipova.Contains(tp))
                    Manifestacija.ListaTipova.Remove(tp);

            // sve objekte unutar listeZaBrisanje uklanjamo iz ListaManifestacija
            foreach (Tip tip in listaZaBrisanje)
            {
                Tipovi.Remove(tip);
                Manifestacija.ListaTipova.Remove(tip);
            }
        }

        private void BtnKreirajTip_Click(object sender, RoutedEventArgs e)
        {
            Tip t = new Tip() { Ime = "Tip " + (Tipovi.Count + 1) };
            Tipovi.Add(t);
            Manifestacija.ListaTipova.Add(t);

            //UnselectTreeViewItem(trvManifestacije);
        }

        private void UnselectTreeViewItem(TreeView pTreeView)
        {
            if (pTreeView.SelectedItem == null)
                return;

            if (pTreeView.SelectedItem is TreeViewItem)
            {
                (pTreeView.SelectedItem as TreeViewItem).IsSelected = false;
            }
            else
            {
                TreeViewItem item = pTreeView.ItemContainerGenerator.ContainerFromIndex(1) as TreeViewItem;
                if (item != null)
                {
                    item.IsSelected = true;
                    item.IsSelected = false;
                }
            }
        }


        private void UcitajTikonu_Click(object sender, RoutedEventArgs e)
        {
            Tip t = (Tip)listview_tipovi.SelectedItem;

            OpenFileDialog dg = new OpenFileDialog
            {
                Title = "Otvori sliku",
                Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png"
            };

            // TODO 1: Proveriti da li ovde moze doci do izuzetka
            dg.ShowDialog();

            t.Ikonica = dg.FileName;
        }

        private void UcitajBoju_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnKreirajEtiketu_Click(object sender, RoutedEventArgs e)
        {
            Etiketa et = new Etiketa() { Oznaka = "Etiketa " + (Etikete.Count + 1) };
            Etikete.Add(et);
            
            Manifestacija.ListaEtiketa.Add(et);
        }

        private void btnUkloniEtiketu_Click(object sender, RoutedEventArgs e)
        {
            List<Etiketa> listaZaBrisanje = new List<Etiketa>();
            IList selektovane = listview_etikete.SelectedItems;

            // uzimamo selektovane objekte iz liste
            foreach (Etiketa etiketa in selektovane)
                listaZaBrisanje.Add(etiketa);

            foreach (Mapa mp in Mape)
                foreach (Manifestacija mf in mp.Manifestacije)
                    foreach (Etiketa st in listaZaBrisanje)
                        if (mf.Etiketee.Contains(st))
                            mf.Etiketee.Remove(st);
            
            // sve objekte unutar listeZaBrisanje uklanjamo iz ListaManifestacija
            foreach (Etiketa etiketa in listaZaBrisanje)
            {
                Etikete.Remove(etiketa);
                Manifestacija.ListaEtiketa.Remove(etiketa);
            }
                
        }

        private void Boja_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            
        }


        ////////////////// SERIJALIZAZICIJA
        private void SacuvajManifestacije_Click(object sender, RoutedEventArgs e)
        {
            //foreach (Mapa m in Mape)
            //    foreach (Manifestacija manifestacija in m.Manifestacije)
            //        manifestacija.IkonicaTipaByteArray = manifestacija.IkonicaZaPrikaz;

            XmlSerializer mySerializer = new XmlSerializer(typeof(ObservableCollection<Mapa>));

            StreamWriter myWriter = new StreamWriter("podaci.xml");

            mySerializer.Serialize(myWriter, Mape);

            myWriter.Close();
        }

        private void UcitajManifestacije_Click(object sender, RoutedEventArgs e)
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(ObservableCollection<Mapa>));
            FileStream myFileStream = new FileStream("podaci.xml", FileMode.Open);

            ObservableCollection<Mapa> lista = mySerializer.Deserialize(myFileStream) as ObservableCollection<Mapa>;

            foreach (Mapa clan in lista)
            {
                //Mape.Clear();
                Mape.Add(clan);
            }

        }

        private void SacuvajTipove_Click(object sender, RoutedEventArgs e)
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(ObservableCollection<Tip>));
 
            StreamWriter myWriter = new StreamWriter("tipovi.xml");

            mySerializer.Serialize(myWriter, Tipovi);

            myWriter.Close();
        }

        private void UcitajTipove_Click(object sender, RoutedEventArgs e)
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(ObservableCollection<Tip>));
            FileStream myFileStream = new FileStream("tipovi.xml", FileMode.Open);

            ObservableCollection<Tip> lista = mySerializer.Deserialize(myFileStream) as ObservableCollection<Tip>;

            foreach (Tip clan in lista)
            {
                Tipovi.Add(clan);
                Manifestacija.ListaTipova.Add(clan);
                
            }
        }

        private void SacuvajEtikete_Click(object sender, RoutedEventArgs e)
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(ObservableCollection<Etiketa>));

            StreamWriter myWriter = new StreamWriter("etikete.xml");

            mySerializer.Serialize(myWriter, Etikete);

            myWriter.Close();
        }

        private void UcitajEtikete_Click(object sender, RoutedEventArgs e)
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(ObservableCollection<Etiketa>));
            FileStream myFileStream = new FileStream("etikete.xml", FileMode.Open);

            ObservableCollection<Etiketa> lista = mySerializer.Deserialize(myFileStream) as ObservableCollection<Etiketa>;

            foreach (Etiketa clan in lista)
            {
                Etikete.Add(clan);
                Manifestacija.ListaEtiketa.Add(clan);
            }
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            // Mape i manifestacije
            XmlSerializer mySerializer = new XmlSerializer(typeof(ObservableCollection<Mapa>));

            StreamWriter myWriter = new StreamWriter("podaci.xml");

            mySerializer.Serialize(myWriter, Mape);

            myWriter.Close();

            // Tipovi
            XmlSerializer mySerializer2 = new XmlSerializer(typeof(ObservableCollection<Tip>));

            StreamWriter myWriter2 = new StreamWriter("tipovi.xml");

            mySerializer2.Serialize(myWriter2, Tipovi);

            myWriter2.Close();

            // Etikete
            XmlSerializer mySerializer3 = new XmlSerializer(typeof(ObservableCollection<Etiketa>));

            StreamWriter myWriter3 = new StreamWriter("etikete.xml");

            mySerializer3.Serialize(myWriter3, Etikete);

            myWriter3.Close();

        }

        private void Otvori_Click(object sender, RoutedEventArgs e)
        {


            XmlSerializer mySerializer = new XmlSerializer(typeof(ObservableCollection<Mapa>));
            FileStream myFileStream = new FileStream("podaci.xml", FileMode.Open);
            XmlSerializer mySerializer2 = new XmlSerializer(typeof(ObservableCollection<Tip>));
            FileStream myFileStream2 = new FileStream("tipovi.xml", FileMode.Open);
            XmlSerializer mySerializer3 = new XmlSerializer(typeof(ObservableCollection<Etiketa>));
            FileStream myFileStream3 = new FileStream("etikete.xml", FileMode.Open);


            ObservableCollection<Mapa> lista = mySerializer.Deserialize(myFileStream) as ObservableCollection<Mapa>;
            ObservableCollection<Tip> lista2 = mySerializer2.Deserialize(myFileStream2) as ObservableCollection<Tip>;
            ObservableCollection<Etiketa> lista3 = mySerializer3.Deserialize(myFileStream3) as ObservableCollection<Etiketa>;

            Tipovi.Clear();
            Etikete.Clear();
            foreach (Mapa clan in Mape)
            {
                clan.Manifestacije.Clear();
            }
            Mape.Clear();

            // Tipovi
            foreach (Tip clan in lista2)
            {
                Tipovi.Add(clan);
                Manifestacija.ListaTipova.Add(clan);
            }

            // Etikete
            foreach (Etiketa clan in lista3)
            {
                Etikete.Add(clan);
                Manifestacija.ListaEtiketa.Add(clan);
            }

            // Mape i manifestacije
            foreach (Mapa clan in lista)
            {
                Mape.Add(clan);
                foreach (Manifestacija m in clan.Manifestacije)
                {
                }
                    
            }
            
        }

        // ------KRAJ KODA ZA OBRADU PODATAKA 

        // DRAG N DROP

        private BitmapImage LoadImageFromFile(string filename)
        {
            using (var fs = File.OpenRead(filename))
            {
                var img = new BitmapImage();
                img.BeginInit();
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.StreamSource = fs;
                img.EndInit();
                return img;
            }
        }

        private void ListView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }

        private void ListView_MouseMove(object sender, MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;

            if (ListaManifestacija.Count != 0)
            {
                if (e.LeftButton == MouseButtonState.Pressed &&
                    (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
                {
                    // Get the dragged ListViewItem
                    //MessageBox.Show("Poslato: " + sender);
                    ListView listView = sender as ListView;
                    //MessageBox.Show("Kastovano na treeView: " + treeView);
                    ListViewItem listViewItem =
                        FindAncestor<ListViewItem>((DependencyObject)e.OriginalSource);

                    //MessageBox.Show("Ja se zovem: " + treeViewItem.Header);

                    // Find the data behind the ListViewItem
                    Manifestacija manifestacija = (Manifestacija)listView.ItemContainerGenerator.ItemFromContainer(listViewItem);

                    // Initialize the drag & drop operation
                    DataObject dragData = new DataObject("myFormat", manifestacija);
                    DragDrop.DoDragDrop(listViewItem, dragData, DragDropEffects.Move);
                }
            }
        }

        private static T FindAncestor<T>(DependencyObject current) where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
                //MessageBox.Show("Trenutni je: " + current);
            }
            while (current != null);
            return null;
        }

        private void platno_DragEnter(object sender, DragEventArgs e)
        {
            //if (!e.Data.GetDataPresent("myFormat") || sender == e.Source)
            //{
            //    e.Effects = DragDropEffects.None;
            //}
            e.Effects = DragDropEffects.Copy;
        }

        private void platno_OnDrop(object sender, DragEventArgs e)
        {
            //if (e.Data.GetDataPresent("myFormat"))
            //{
            //    Manifestacija manifestacija = e.Data.GetData("myFormat") as Manifestacija;
            //    //Mapa manifestacija = e.Data.GetData("myFormat") as Mapa;
            //    // TODO 3: ovde implementirati logiku za otkrivanje indeksa mape koja sadrzi odabranu kolekciju

            //    //Mape[0].Manifestacije.Remove(manifestacija);
            //    Manifestacije2.Add(manifestacija);
            //}

            var data = e.Data as DataObject;

            if (e.Data.GetDataPresent("myFormat"))
            {
                Manifestacija manifestacija = new Manifestacija();
                manifestacija = e.Data.GetData("myFormat") as Manifestacija;


                Image ikonica = new Image();
                // postavljanje pin-a kao slike u Image tag
                //ikonica.Source = LoadImageFromFile(manifestacija.Ikonica);
                // velicina pin-a na mapi
                ikonica.Width = 48;
                ikonica.Height = 48;
                // pozicioniranje pin-a nakon otpustanja levog tastera misa iznad mape
                taboviSirina = tabovi.ActualWidth;
                lastX = e.GetPosition(this).X + promenaSkrolaHorizontalna - taboviSirina;
                lastY = e.GetPosition(this).Y + promenaSkrolaVertikalna - TopMenu.ActualHeight;
                // postavljanje pina unutar platna
                ikonica.Visibility = Visibility.Visible;

                // provera da li ikonica vec postoji na mapi
                // TODO 1: Onemogućiti dodavanje ikonice iste manifestacije više od jednog puta na mapu

                manifestacija.UcitanaIkonica.ToolTip = manifestacija.Ime + " (" + manifestacija.Oznaka + ")";

                if (!platno.Children.Contains(manifestacija.UcitanaIkonica)) {
                    manifestacija.UcitanaIkonica.Width = 48;
                    manifestacija.UcitanaIkonica.Height = 48;
                    platno.Children.Add(manifestacija.UcitanaIkonica);
                    Canvas.SetTop(manifestacija.UcitanaIkonica, lastY - 24);
                    Canvas.SetLeft(manifestacija.UcitanaIkonica, lastX - 24);
                }
                else
                    MessageBox.Show("Nije moguće više puta dodati istu manifestaciju na mapu");


                

            }
        }

        private void platno_DragOver(object sender, DragEventArgs e)
        {
            // dobavljamo trenutnu poziciju kursora prilikom pomeranja misa nad mapom
            currentX = e.GetPosition(this).X;
            currentY = e.GetPosition(this).Y;
        }

        private void skrol_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            promenaSkrolaHorizontalna = e.HorizontalOffset;
            promenaSkrolaVertikalna = e.VerticalOffset;
        }

        private void platno_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lokacijaKlikaNaMapi = new Point(e.GetPosition(null).X, e.GetPosition(null).Y);
            double y = 0;
            double x = 0;

            //this.CaptureMouse();

            //this.Width = 350;
            //this.Height = 350;

            //this.ReleaseMouseCapture();

            foreach (UIElement ikonica in platno.Children)
            {
                    // example 0
                    y = Canvas.GetTop(ikonica);
                    x = Canvas.GetLeft(ikonica);
                

            }

            //platno.Children.IndexOf()

            //MessageBox.Show("X = " + x + " Y = " + y);

            if (e.OriginalSource is Image)
            {
                Image ClickedRectangle = (Image)e.OriginalSource;

                // Your work here I give y some actions ...
                //ClickedRectangle.Opacity = 0.5;

                foreach (Manifestacija m in ListaManifestacija)
                {
                    if (m.UcitanaIkonica == ClickedRectangle)
                    {
                        // uradi nesto sa pinom klinute manifestacije
                    }
                }



                if (e.ClickCount == 2)
                {
                    platno.Children.Remove(ClickedRectangle);
                }
            }
        }
    }
}
