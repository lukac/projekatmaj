﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;

namespace projekat.model
{
    public enum prostorEnum { Zatvoreno = 0, Otvoreno };
    
    public class Manifestacija : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string naziv)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(naziv));
            }
        }
        


        static event PropertyChangedEventHandler GlobalPropertyChanged = delegate { };
        static void OnGlobalPropertyChanged(string propertyName)
        {
            GlobalPropertyChanged(typeof(Tip), new PropertyChangedEventArgs(propertyName));
        }


        private byte[] ikonicaTipaByteArray;
        [XmlIgnore]
        private Image ucitanaIkonica;
        [XmlIgnore]
        private BitmapImage ikonicaZaPrikaz;
        private string oznaka;
        private string ime;
        private string opis;
        private Tip tip;
        private string alkohol;
        private string ikonica;
        private bool hendikep;
        private bool pusenje;
        private prostorEnum prostor;
        private string cena;
        private string publika;
        private DateTime datum;
        private Etiketa etiketa;
        private List<Etiketa> etiketee = new List<Etiketa>();
        [XmlIgnore]
        private List<string> listaStatusaAlkohola = new List<string>();
        [XmlIgnore]
        private List<string> listaStatusaCena = new List<string>();
        [XmlIgnore]
        private List<string> listaStatusaPublike = new List<string>();
        private static List<Tip> listaTipova = new List<Tip>();
        private static List<Etiketa> listaEtiketa = new List<Etiketa>();


        public Manifestacija() {
            
            ListaStatusaAlkohola.Add("Nema alkohola");
            ListaStatusaAlkohola.Add("Može se doneti");
            ListaStatusaAlkohola.Add("Može se kupiti na licu mesta");

            ListaStatusaCena.Add("Besplatno");
            ListaStatusaCena.Add("Niske cene");
            ListaStatusaCena.Add("Srednje cene");
            ListaStatusaCena.Add("Visoke cene");

            ListaStatusaPublike.Add("< 100");
            ListaStatusaPublike.Add("100 - 1000");
            ListaStatusaPublike.Add("1000 - 5000");
            ListaStatusaPublike.Add("5000 - 10000");
            ListaStatusaPublike.Add("10000 - 50000");
            ListaStatusaPublike.Add("> 50000");

            Datum = System.DateTime.Now;
        }
        [XmlIgnore]
        public Image UcitanaIkonica
        {
            get
            {
                return ucitanaIkonica;
            }
            set
            {
                if (value != ucitanaIkonica)
                {
                    ucitanaIkonica = value;
                    OnPropertyChanged("UcitanaIkonica");
                }
            }
        }

        [XmlIgnore]
        public BitmapImage IkonicaZaPrikaz
        {
            get
            {
                return ikonicaZaPrikaz;
            }
            set
            {
                if (value != ikonicaZaPrikaz)
                {
                    ikonicaZaPrikaz = value;
                    OnPropertyChanged("IkonicaZaPrikaz");
                }
            }
        }

        public string Oznaka
        {
            get
            {
                return oznaka;
            }
            set
            {
                if (value != oznaka)
                {
                    oznaka = value;
                    OnPropertyChanged("Oznaka");
                }
            }
        }
        public string Ime
        {
            get
            {
                return ime;
            }
            set
            {
                if (value != ime)
                {
                    ime = value;
                    OnPropertyChanged("Ime");
                }
            }
        }
        public string Opis
        {
            get
            {
                return opis;
            }
            set
            {
                if (value != opis)
                {
                    opis = value;
                    OnPropertyChanged("Opis");
                }
            }
        }
        public Tip Tip
        {
            get
            {
                return tip;
            }

            set
            {
                if (value != tip)
                {
                    tip = value;
                    OnPropertyChanged("Tip");
                }
            }
        }
        
        public string Alkohol
        {
            get
            {
                return alkohol;
            }
            set
            {
                if (value != alkohol)
                {
                    alkohol = value;
                    OnPropertyChanged("Alkohol");
                }
            }
        }
        public string Ikonica
        {
            get
            {
                return ikonica;
            }
            set
            {
                if (value != ikonica)
                {
                    ikonica = value;
                    OnPropertyChanged("Ikonica");
                }
            }
        }
        public bool Hendikep
        {
            get
            {
                return hendikep;
            }
            set
            {
                if (value != hendikep)
                {
                    hendikep = value;
                    OnPropertyChanged("Hendikep");
                }
            }
        }
        public bool Pusenje
        {
            get
            {
                return pusenje;
            }
            set
            {
                if (value != pusenje)
                {
                    pusenje = value;
                    OnPropertyChanged("Pusenje");
                }
            }
        }
        public prostorEnum Prostor
        {
            get
            {
                return prostor;
            }
            set
            {
                if (value != prostor)
                {
                    prostor = value;
                    OnPropertyChanged("Prostor");
                }
            }
        }
        public string Cena
        {
            get
            {
                return cena;
            }
            set
            {
                if (value != cena)
                {
                    cena = value;
                    OnPropertyChanged("Cena");
                }
            }
        }
        public string Publika
        {
            get
            {
                return publika;
            }
            set
            {
                if (value != publika)
                {
                    publika = value;
                    OnPropertyChanged("Publika");
                }
            }
        }
        public DateTime Datum
        {
            get
            {
                return datum;
            }
            set
            {
                if (value != datum)
                {
                    datum = value;
                    OnPropertyChanged("Datum");
                }
            }
        }
        public Etiketa Etiketa        {
            get
            {
                return etiketa;
            }
            set
            {
                if (!Etiketee.Contains(value))
                {
                    Etiketee.Add(value);
                    OnPropertyChanged("Etiketa");
                }
            }
        }
        public List<Etiketa> Etiketee
        {
            get
            {
                return etiketee;
            }
        }
        [XmlIgnore]
        public List<string> ListaStatusaCena { get => listaStatusaCena; set => listaStatusaCena = value; }
        [XmlIgnore]
        public List<string> ListaStatusaAlkohola { get => listaStatusaAlkohola; set => listaStatusaAlkohola = value; }
        [XmlIgnore]
        public List<string> ListaStatusaPublike { get => listaStatusaPublike; set => listaStatusaPublike = value; }





        public static List<Tip> ListaTipova
        {
            get
            {
                return listaTipova;
            }
            set
            {
                if (value != listaTipova)
                {
                    listaTipova = value;
                    OnGlobalPropertyChanged("ListaTipova");
                }
            }
        }

        public static List<Etiketa> ListaEtiketa
        {
            get
            {
                return listaEtiketa;
            }
            set
            {
                if (value != listaEtiketa)
                {
                    listaEtiketa = value;
                    OnGlobalPropertyChanged("ListaEtiketa");
                }
            }
        }
        [XmlElementAttribute("IkonicaZaPrikaz")]
        public byte[] IkonicaTipaByteArray
        {
            get
            {
                if(ikonicaZaPrikaz != null)
                {
                    MemoryStream ms = new MemoryStream();
                    PngBitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(ikonicaZaPrikaz));
                    encoder.Save(ms);

                    byte[] buffer = ms.GetBuffer();
                    return buffer;
                } else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    ikonicaZaPrikaz = new BitmapImage();
                    ikonicaZaPrikaz.StreamSource = new System.IO.MemoryStream(value);
                } else
                {
                    ikonicaZaPrikaz = null;
                }
            }
        }





    }
}
