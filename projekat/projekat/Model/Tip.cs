﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;

namespace projekat.model
{
    public class Tip : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string naziv)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(naziv));
            }
        }
        
        private string oznaka;
        private string ime;
        private string ikonica;
        private string opis;

        public string Oznaka
        {
            get
            {
                return oznaka;
            }
            set
            {
                if (value != oznaka)
                {
                    oznaka = value;
                    OnPropertyChanged("Oznaka");
                }
            }
        }
        public string Ime
        {
            get
            {
                return ime;
            }
            set
            {
                if (value != ime)
                {
                    ime = value;
                    OnPropertyChanged("Ime");
                }
            }
        }
        public string Ikonica
        {
            get
            {
                return ikonica;
            }
            set
            {
                if (value != ikonica)
                {
                    ikonica = value;
                    OnPropertyChanged("Ikonica");
                }
            }
        }
        public string Opis
        {
            get
            {
                return opis;
            }
            set
            {
                if (value != opis)
                {
                    opis = value;
                    OnPropertyChanged("Opis");
                }
            }
        }

        ////////////////////////////////////////////////////////////////
        public override bool Equals(Object obj)
        {
            Tip other = obj as Tip;
            if (other == null)
                return false;
            else
            {
                bool eq = false;


                // TODO 2: implementirati trajnu logiku poredjenja nakon sto se obezbedi obavezna jedinstvena oznaka
                if (this.oznaka != null && other.oznaka != null && this.oznaka.Equals(other.oznaka))
                    eq = true;
                else if (this.oznaka == null && other.oznaka == null)
                    if (this.ime.Equals(other.ime))
                        eq = true;

                return eq;
            }
        }
    }
}
