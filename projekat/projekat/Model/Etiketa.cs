﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Serialization;
using Xceed.Wpf.Toolkit;

namespace projekat.model
{
    public class Etiketa : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string naziv)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(naziv));
            }
        }

        private string oznaka;
        
        private string opis;
        [XmlIgnore]
        public ObservableCollection<ColorItem> ListaBoja { get; set; }

        public Etiketa()
        {

            ListaBoja = new ObservableCollection<ColorItem>();

            ListaBoja.Add(new ColorItem(Colors.Blue, "Blue"));
            ListaBoja.Add(new ColorItem(Colors.Green, "Green"));
            ListaBoja.Add(new ColorItem(Colors.Red, "Red"));
            ListaBoja.Add(new ColorItem(Colors.Yellow, "Yellow"));
            ListaBoja.Add(new ColorItem(Colors.Black, "Black"));
        }

        public string Oznaka
        {
            get
            {
                return oznaka;
            }
            set
            {
                if (value != oznaka)
                {
                    oznaka = value;
                    OnPropertyChanged("Oznaka");
                }
            }
        }
        [XmlIgnore]
        private ColorItem boja;
        [XmlIgnore]
        public ColorItem Boja
        {
            get
            {
                return boja;
            }
            set
            {
                if (value != boja)
                {
                    boja = value;
                    OnPropertyChanged("Boja");
                    MessageBox.Show("Promenio si boju!");
                }
            }
        }

        public string Opis
        {
            get
            {
                return opis;
            }
            set
            {
                if (value != opis)
                {
                    opis = value;
                    OnPropertyChanged("Opis");
                }
            }
        }

        public override bool Equals(Object obj)
        {
            Etiketa other = obj as Etiketa;
            if (other == null)
                return false;
            else
            {
                return this.oznaka.Equals(other.oznaka);
            }
        }

        public static explicit operator Etiketa(List<Etiketa> v)
        {
            throw new NotImplementedException();
        }
    }
}
