﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;

namespace projekat.model
{

    public class Mapa : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private string naziv;
        public string Naziv
        {
            get
            {
                return naziv;
            }
            set
            {
                if (naziv != value)
                {
                    naziv = value;
                    OnPropertyChanged("Naziv");
                }
            }
        }

        private string slikaMape;
        public string SlikaMape
        {
            get
            {
                return slikaMape;
            }
            set
            {
                if (slikaMape != value)
                {
                    slikaMape = value;
                    OnPropertyChanged("SlikaMape");
                }
            }
        }

        [XmlIgnore]
        private Canvas platnoo;
        [XmlIgnore]
        public Canvas Platnoo
        {
            get
            {
                return platnoo;
            }
            set
            {
                if (platnoo != value)
                {
                    platnoo = value;
                    OnPropertyChanged("Platnoo");
                }
            }
        }
        [XmlIgnore]
        private Image ucitanaSlikaMape;
        [XmlIgnore]
        public Image UcitanaSlikaMape
        {
            get
            {
                return ucitanaSlikaMape;
            }
            set
            {
                if (ucitanaSlikaMape != value)
                {
                    ucitanaSlikaMape = value;
                    OnPropertyChanged("UcitanaSlikaMape");
                }
            }
        }

        public ObservableCollection<Manifestacija> Manifestacije
        {
            get;
            set;
        }

        public Mapa()
        {
            Manifestacije = new ObservableCollection<Manifestacija>();
        }


        private BitmapImage LoadImageFromFile(string filename)
        {
            using (var fs = File.OpenRead(filename))
            {
                var img = new BitmapImage();
                img.BeginInit();
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.StreamSource = fs;
                img.EndInit();
                return img;
            }
        }
    }
}
